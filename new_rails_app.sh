#!/bin/bash

GREEN='\033[0;32m'
RED="\033[0;31m"
NC='\033[0m' # No Color

# TODO: use real template
TEMPLATE_PATH='template_test.rb'
RAILS_PARAMS="-m $TEMPLATE_PATH --skip-webpack-install --skip-bundle --rspec"

printf "%.s/" {1..80}
echo
printf "%.s " {1..33}
echo "NEW RAILS APP"
printf "%.s/" {1..80}
echo
echo "This script will create a new Rails app in $PWD."
echo
echo

# App name
#---------
while [[ ! $app_name ]]; do
  read -p "App name: " app_name
  # [[ -d "$app_name" ]] && echo -e "${RED}/$app_name already exists.${NC}" && unset app_name
done
mkdir $app_name
mkdir $app_name/lib
mkdir $app_name/lib/rails_template
touch $app_name/lib/rails_template/template_preferences.yml
pref_file=$app_name/lib/rails_template/template_preferences.yml
echo -e "${GREEN}$app_name directory created${NC}"
echo

##################
# TODO
# clone the template into application/lib/
##################

# Test framework
#---------------
unset choice
while [[ ! ${choice} =~ [0-2] ]]; do
  printf "Test framework (Default Rspec):\n0 - None\n1 - Rspec\n2 - Minitest"
  echo
  read -p "> " choice
  [[ $choice ]] || choice=1
done
case $choice in
  0)
    RAILS_PARAMS+=" -T"
    ;;
  1)
    echo "rspec: true" >> $pref_file
    RAILS_PARAMS+=" -T"
    ;;
esac

# Database
#---------
choices=("postgresql" "mysql" "sqlite3" "mongodb")
unset choice
while [[ ! ${choice} =~ [0-3] ]]; do
  echo "Database (Default Postgresql):"
  for ((i = 0; i < ${#choices[@]}; ++i)); do
    v=${choices[$i]}
    v=$(tr a-z A-Z <<< ${v:0:1})${v:1}
    echo "$i - $v"
  done
  echo
  read -p "> " choice
  [[ $choice ]] || choice=0
done
case $choice in
  [0-2])
    RAILS_PARAMS+=" -d ${choices[$choice]}"
    ;;
  3)
    echo "database: mongodb" >> $pref_file
    RAILS_PARAMS+=" -d ${choices[$choice]}"
    RAILS_PARAMS+=" -O"
    ;;
esac

# Api only
#---------
unset choice
while [[ ! ${choice} =~ [YyNn] ]]; do
  echo "Do you wish create a API only app? (y/n - Default no)"
  read -p "> " choice
  [[ $choice ]] || choice=n

  case $choice in
    [Yy]* ) api="true";;
    [Nn]* ) api="false";;
  esac
done
if [[ $api = "true" ]]; then
  RAILS_PARAMS+=" --api"
fi
echo "api_only: $api" >> $pref_file

# Webpack
#--------
# skip if API only
choices=("no framework" "react" "vue" "angular" "elm" "stimulus" "skip Webpack")
unset choice
while [[ ! ${choice} =~ [0-6] ]]; do
  echo "Webpack options (Default no framework):"
  for ((i = 0; i < ${#choices[@]}; ++i)); do
    v=${choices[$i]}
    v=$(tr a-z A-Z <<< ${v:0:1})${v:1}
    echo "$i - $v"
  done
  echo
  read -p "> " choice
  [[ $choice ]] || choice=0
done
case $choice in
  [1-5])
    RAILS_PARAMS+=" --webpacker ${choices[$choice]}"
    ;;
  6)
    RAILS_PARAMS+=" --skip-webpack-install"
    ;;
esac

# Git
#----
unset choice
while [[ ! ${choice} =~ [YyNn] ]]; do
  echo "Do you wish to use git? (y/n - Default yes)"
  read -p "> " choice
  [[ $choice ]] || choice=y

  case $choice in
    [Yy]* ) git="true";;
    [Nn]* ) git="false";;
  esac
done
if [[ $git = "false" ]]; then
  RAILS_PARAMS+=" --skip-git"
fi
echo "git: $git" >> $pref_file

# Rvm
#----
unset choice
while rvm &> /dev/null && [[ ! ${choice} =~ [YyNn] ]]; do
  echo "Do you wish use RVM to manage your version of Ruby? (y/n - Default yes)"
  read -p "> " choice
  [[ $choice ]] || choice=y

  case $choice in
    [Yy]* ) rvm="true";;
    [Nn]* ) rvm="false";;
  esac
done
if [[ $rvm = "true" ]]; then
  source ~/.rvm/scripts/rvm
  rvm get stable
  unset choice
  while [[ ! ${choice} =~ ^([0-9].[0-9].[0-9]|--default)+$ ]]; do
    echo "Which version of Ruby do you wish to use? (blank: latest stable)"
    read -p "> " choice
    [[ $choice ]] || choice="--default"
  done
  rvm use ruby --install $choice
  rvm gemset create $app_name
  rvm gemset use $app_name
  echo "$app_name" > $app_name/.ruby-gemset
fi

# Install Rails
#--------------
no_doc='gem: --no-document'
touch -a ~/.gemrc
grep -qF "$no_doc" ~/.gemrc || echo "$no_doc" >> ~/.gemrc
gem install rails --conservative --no-document
echo 'Creating $app_name rails applications using params: $RAILS_PARAMS'
rails new $app_name $RAILS_PARAMS
