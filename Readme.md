# Rails application generator

This repo contains a script that will create a gemfile and call the Rails with a template.

## Usage

Executing the setup script will create a new Rails app in the current directory.
```
bash <(curl -s https://gitlab.com/thomas_g/rails-template/-/raw/master/new_rails_app.sh)
```

## Ressources

Some exemples: https://github.com/jm/rails-templates

## TODO

- Create template rails
- Download ruby template files and config files to /lib/rails_template/
- Add bullet gem: https://github.com/flyerhzm/bullet
